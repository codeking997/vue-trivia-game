# vue-trivia-game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Development Git maners

- merge master into your branch before merging it into master. This is to be 100% sure it works before a merge/pull request to master.
- branch ofthen and name your branch accordingly i.e "test-development-thomas" ( include your name at the end of a branch name)
- create a pull request on gitlab if you want to have other developers review your code before merging, and to make sure the code works on other computers because JavaScript.
